Const $BUTTON_SIZE = 50, $MINE_RATIO = 0.15, $WIDTH = 800, $HEIGHT = 600
Const $COUNT_X = Floor($WIDTH / $BUTTON_SIZE), $COUNT_Y = Floor($HEIGHT / $BUTTON_SIZE)
Const $TOTAL = $countX * $COUNT_Y, $mineCount = Round($TOTAL * $MINE_RATIO)
Global $visibleCount, $button[$countX][$COUNT_Y], $value[$countX][$COUNT_Y], $window = GUICreate("Minesweeper", $countX * $BUTTON_SIZE, $COUNT_Y * $BUTTON_SIZE), $msg

initialize()
While $msg <> -3 ; #include <GUIConstantsEx.au3> --> $GUI_EVENT_CLOSE
	$msg = GUIGetMsg()
	For $x = 0 To $countX - 1
		For $y = 0 To $COUNT_Y - 1
			If $msg = $button[$x][$y] Then
				If Not (GUICtrlRead($button[$x][$y])) Then
					GUICtrlSetData($button[$x][$y], $value[$x][$y])
					$visibleCount += 1
					If IsNumber($value[$x][$y]) Then
						If $value[$x][$y] = 0 Then revealNeighbors($x, $y)
						If $visibleCount = $TOTAL - $mineCount Then
							MsgBox(0, "win", "lucker!", 1.5)
							initialize()
						EndIf
					Else
						explosion($x, $y)
						MsgBox(0, "game over", "don't be a looser, buy a defuser!", 1.5)
						initialize()
					EndIf
				EndIf
			EndIf
		Next
	Next
WEnd ;==>main loop

Func initialize()
	$visibleCount = 0
	$msg = 0
	For $x = 0 To $countX - 1
		For $y = 0 To $COUNT_Y - 1
			$value[$x][$y] = 0
			GUICtrlDelete($button[$x][$y])
			$button[$x][$y] = GUICtrlCreateButton("", $x * $BUTTON_SIZE, $y * $BUTTON_SIZE, $BUTTON_SIZE, $BUTTON_SIZE)
		Next
	Next
	plantMines()
	GUISetState(@SW_SHOW)
EndFunc   ;==>initialize

Func revealNeighbors($i, $j)
	For $x = ($i - 1) To ($i + 1)
		For $y = ($j - 1) To ($j + 1)
			If $x > -1 And $y > -1 And $x < $countX And $y < $COUNT_Y And IsNumber($value[$x][$y]) And Not (GUICtrlRead($button[$x][$y])) Then
				$visibleCount += 1
				GUICtrlSetData($button[$x][$y], $value[$x][$y])
				If $value[$x][$y] = 0 Then revealNeighbors($x, $y)
			EndIf
		Next
	Next
EndFunc   ;==>revealNeighbors

Func explosion($x, $y)
    const $chars[8] = ["\", "-", "/", "|", "|", "/", "-", "\"]
	$i = 0
	For $a = ($x - 1) To ($x + 1)
		For $b = ($y - 1) To ($y + 1)
		    If $a > -1 And $a < $countX And $b > -1 And $b < $COUNT_Y And ($a <> $x Or $b <> $y) Then GUICtrlSetData($button[$a][$b], $chars[$i])
			If ($a <> $x Or $b <> $y) Then $i += 1
		Next
	 Next
EndFunc   ;==>explosion

Func plantMines()
	For $i = 1 To $mineCount
		Do
			$x = Random(0, $countX - 1, 1)
			$y = Random(0, $COUNT_Y - 1, 1)
		Until Not ($value[$x][$y] == "#")
		$value[$x][$y] = "#"

	    For $a = ($x - 1) To ($x + 1)
			For $b = ($y - 1) To ($y + 1)
			   If $a > -1 And $a < $countX And $b > -1 And $b < $COUNT_Y And IsNumber($value[$a][$b]) Then $value[$a][$b] += 1
			Next
	    Next
	Next
EndFunc   ;==>plantMines
